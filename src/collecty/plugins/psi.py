#!/usr/bin/python3
###############################################################################
#                                                                             #
# collecty - A system statistics collection daemon for IPFire                 #
# Copyright (C) 2021 IPFire development team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import os

from . import base
from ..colours import *
from ..constants import *
from ..i18n import _

class PSIGraphTemplate(base.GraphTemplate):
	name = "psi"

	@property
	def rrd_graph(self):
		rrd_graph = [
			"LINE2:some_avg300%s:%s" % (
				YELLOW, LABEL % _("5 Minutes"),
			),
			"GPRINT:some_avg300_cur:%s" % FLOAT,
			"GPRINT:some_avg300_avg:%s" % FLOAT,
			"GPRINT:some_avg300_min:%s" % FLOAT,
			"GPRINT:some_avg300_max:%s\\j" % FLOAT,

			"LINE2:some_avg60%s:%s" % (
				ORANGE, LABEL % _("1 Minute"),
			),
			"GPRINT:some_avg60_cur:%s" % FLOAT,
			"GPRINT:some_avg60_avg:%s" % FLOAT,
			"GPRINT:some_avg60_min:%s" % FLOAT,
			"GPRINT:some_avg60_max:%s\\j" % FLOAT,

			"LINE2:some_avg10%s:%s" % (
				RED, LABEL % _("10 Seconds"),
			),
			"GPRINT:some_avg10_cur:%s" % FLOAT,
			"GPRINT:some_avg10_avg:%s" % FLOAT,
			"GPRINT:some_avg10_min:%s" % FLOAT,
			"GPRINT:some_avg10_max:%s\\j" % FLOAT,

			# Headline
			"COMMENT:%s" % EMPTY_LABEL,
			"COMMENT:%s" % (COLUMN % _("Current")),
			"COMMENT:%s" % (COLUMN % _("Average")),
			"COMMENT:%s" % (COLUMN % _("Minimum")),
			"COMMENT:%s\\j" % (COLUMN % _("Maximum")),
		]

		return rrd_graph

	upper_limit = 100
	lower_limit = 0

	@property
	def graph_title(self):
		titles = {
			"cpu"    : _("Processor Pressure Stall Information"),
			"io"     : _("Input/Output Pressure Stall Information"),
			"memory" : _("Memory Pressure Stall Information"),
		}

		try:
			return titles[self.object.id]
		except KeyError:
			return _("%s Pressure Stall Information") % self.object.id

	@property
	def graph_vertical_label(self):
		return _("Percentage")

	@property
	def rrd_graph_args(self):
		return [
			"--legend-direction=bottomup",
		]


class PSIObject(base.Object):
	rrd_schema = [
		# some
		"DS:some_avg10:GAUGE:0:100",
		"DS:some_avg60:GAUGE:0:100",
		"DS:some_avg300:GAUGE:0:100",
		"DS:some_total:DERIVE:0:U",

		# full
		"DS:full_avg10:GAUGE:0:100",
		"DS:full_avg60:GAUGE:0:100",
		"DS:full_avg300:GAUGE:0:100",
		"DS:full_total:DERIVE:0:U",
	]

	def __repr__(self):
		return "<%s %s>" % (self.__class__.__name__, self.item)

	def init(self, item):
		self.item = item

		self.path = os.path.join("/proc/pressure", self.item)

	@property
	def id(self):
		return self.item

	def collect(self):
		lines = self.read_file("/proc/pressure", self.item)

		# Do nothing if nothing could be read
		if not lines:
			return

		# Parse all input lines
		values = {}
		for line in lines.splitlines():
			values.update(self._parse_psi(line))

		# Return all values in order
		for share in ("some", "full"):
			for value in ("avg10", "avg60", "avg300", "total"):
				yield values.get("%s-%s" % (share, value), None)

	def _parse_psi(self, line):
		words = line.split(" ")

		share = None
		values = {}

		for i, word in enumerate(words):
			# Store the share of time
			if i == 0:
				share = word
				continue

			# Split word
			key, delim, value = word.partition("=")

			# Store it in the values array
			values["%s-%s" % (share, key)] = value

		# Return everything
		return values


class PSIPlugin(base.Plugin):
	name = "psi"
	description = "Pressure Stall Information Plugin"

	templates = [
		PSIGraphTemplate,
	]

	@property
	def objects(self):
		for item in os.listdir("/proc/pressure"):
			yield PSIObject(self, item)
