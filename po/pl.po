# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Fabian <vondor666@gmail.com>, 2014
# Jakub Ratajczak <j.ratajczak@eqba.pl>, 2013
# Przemysław Karpeta <przemyslaw.karpeta@gmail.com>, 2014
msgid ""
msgstr ""
"Project-Id-Version: IPFire Project\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-09-28 17:07+0000\n"
"PO-Revision-Date: 2015-10-26 17:22+0000\n"
"Last-Translator: Michael Tremer <michael.tremer@ipfire.org>\n"
"Language-Team: Polish (http://www.transifex.com/mstremer/ipfire/language/"
"pl/)\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

msgid "Bus thread has started"
msgstr ""

msgid "Bus thread has ended"
msgstr ""

msgid "Stopping bus thread"
msgstr ""

msgid "Collecty successfully initialized"
msgstr ""

#, python-format
msgid "Plugin %s could not be initialised"
msgstr ""

msgid "Main thread exited"
msgstr ""

msgid "Received shutdown signal"
msgstr "Odebrano sygnał zamknięcia"

#, python-format
msgid "Registering signal %d"
msgstr "Rejestracja sygnału %d"

#, python-format
msgid "Caught signal %d"
msgstr "Złapany sygnał %d"

#, python-format
msgid "Backing up to %s..."
msgstr ""

#, python-format
msgid "Adding %s to backup..."
msgstr ""

msgid "Backup finished"
msgstr ""

msgid "Initialised write queue"
msgstr ""

msgid "Committing data to disk..."
msgstr ""

msgid "No data to commit"
msgstr ""

#, python-format
msgid "Emptied write queue in %.2fs"
msgstr ""

#, python-format
msgid "Committing %(counter)s entries to %(filename)s"
msgstr ""

#, python-format
msgid "Could not update RRD database %s: %s"
msgstr ""

#, python-format
msgid "Plugin is not properly configured: %s"
msgstr ""

#, python-format
msgid "Successfully initialized %s"
msgstr ""

#, python-format
msgid "Unhandled exception in %s.collect()"
msgstr ""

#, python-format
msgid "Received empty result: %s"
msgstr ""

#, python-format
msgid "Collected %s: %s"
msgstr ""

#, python-format
msgid "A worker thread was stalled for %.4fs"
msgstr ""

#, python-format
msgid "Collection finished in %.2fms"
msgstr ""

#, python-format
msgid "Generated graph %s in %.1fms"
msgstr ""

#, python-format
msgid "Created RRD file %s."
msgstr "Utworzono plik RRD %s."

#. Brand all generated graphs
msgid "Created by collecty"
msgstr "Stworzony przez collecty"

#, python-format
msgid "Generating graph %s"
msgstr ""

msgid "Current"
msgstr ""

msgid "Average"
msgstr "średnia"

msgid "Minimum"
msgstr "Minimum"

msgid "Maximum"
msgstr "Maximum"

msgid "Entries"
msgstr ""

msgid "Connection Tracking Table"
msgstr ""

msgid "Context Switches"
msgstr ""

msgid "Context Switches/s"
msgstr ""

msgid "Processor Frequencies"
msgstr ""

msgid "Frequency"
msgstr ""

msgid "Hz"
msgstr ""

msgid "Used"
msgstr ""

msgid "Free"
msgstr ""

#, python-format
msgid "Disk Usage of %s"
msgstr ""

msgid "Bytes"
msgstr ""

#, python-format
msgid "Inode Usage of %s"
msgstr ""

msgid "Inodes"
msgstr ""

msgid "Bad Sectors"
msgstr ""

#, python-format
msgid "Bad Sectors of %s"
msgstr ""

msgid "Pending/Relocated Sectors"
msgstr ""

msgid "Read"
msgstr ""

msgid "Written"
msgstr ""

#, python-format
msgid "Disk Utilisation of %s"
msgstr ""

msgid "Byte per Second"
msgstr ""

#, python-format
msgid "Disk IO Operations of %s"
msgstr ""

msgid "Operations per Second"
msgstr ""

msgid "Temperature"
msgstr ""

#, python-format
msgid "Disk Temperature of %s"
msgstr ""

msgid "° Celsius"
msgstr ""

msgid "Received"
msgstr ""

msgid "Transmitted"
msgstr ""

#. Draw the 95% lines.
#, fuzzy
msgid "95th Percentile"
msgstr "procent"

#, python-format
msgid "Bandwidth Usage on %s"
msgstr ""

msgid "Bit/s"
msgstr ""

#, python-format
msgid "Transferred Packets on %s"
msgstr ""

msgid "Packets/s"
msgstr ""

msgid "Receive Errors"
msgstr ""

msgid "Transmit Errors"
msgstr ""

msgid "Receive Drops"
msgstr ""

msgid "Transmit Drops"
msgstr ""

msgid "Collisions"
msgstr ""

#, python-format
msgid "Errors/Dropped Packets on %s"
msgstr ""

#, python-format
msgid "Interface %s does not exists. Cannot collect."
msgstr ""

#, fuzzy
msgid "Interrupts"
msgstr "przerwanie"

#, fuzzy, python-format
msgid "Interrupt %s"
msgstr "przerwanie"

#, fuzzy
msgid "Interrupts/s"
msgstr "przerwanie"

msgid "Failed Reassemblies"
msgstr ""

msgid "Reassembly Timeouts"
msgstr ""

msgid "Successful Reassemblies"
msgstr ""

msgid "Failed Fragmentations"
msgstr ""

msgid "Fragmented Packets"
msgstr ""

#, python-format
msgid "IPv6 Fragmentation on %s"
msgstr ""

msgid "IPv6 Fragmentation"
msgstr ""

#, python-format
msgid "IPv4 Fragmentation on %s"
msgstr ""

msgid "IPv4 Fragmentation"
msgstr ""

#. Colour background on packet loss
msgid "Packet Loss"
msgstr ""

msgid "0-5%"
msgstr ""

msgid "5-10%"
msgstr ""

msgid "10-25%"
msgstr ""

msgid "25-50%"
msgstr ""

msgid "50-99%"
msgstr ""

msgid "Latency (IPv4)"
msgstr ""

msgid "Latency (IPv6)"
msgstr ""

msgid "Default Gateway"
msgstr ""

#, python-format
msgid "Latency to %s"
msgstr ""

msgid "Milliseconds"
msgstr ""

#, python-format
msgid "Could not add host %(host)s for family %(family)s"
msgstr ""

#, python-format
msgid "Could not run latency check for %(host)s: %(msg)s"
msgstr ""

msgid "15 Minutes"
msgstr ""

msgid "5 Minutes"
msgstr ""

msgid "1 Minute"
msgstr ""

#, fuzzy
msgid "Load Average"
msgstr "średnie obciążenie"

msgid "Load"
msgstr "załadować"

#, fuzzy
msgid "Used Memory"
msgstr "używana pamięć"

#, fuzzy
msgid "Buffered Data"
msgstr "buforowane dane"

#, fuzzy
msgid "Cached Data"
msgstr "buforowane danych"

#, fuzzy
msgid "Free Memory"
msgstr "używana pamięć"

#, fuzzy
msgid "Used Swap Space"
msgstr "użycia Partycja wymiany"

msgid "Memory Usage"
msgstr "użycia pamięci"

msgid "Total"
msgstr ""

msgid "User"
msgstr "Użytkownik"

msgid "Nice"
msgstr "Nicea"

msgid "System"
msgstr "System"

msgid "Wait"
msgstr "czekanie"

msgid "Interrupt"
msgstr "przerwanie"

#, fuzzy
msgid "Soft Interrupt"
msgstr "miękkie przerwanie"

msgid "Steal"
msgstr ""

msgid "Guest"
msgstr ""

msgid "Guest Nice"
msgstr ""

#, fuzzy
msgid "Processor Usage"
msgstr "użycia pamięci"

msgid "Percent"
msgstr "procent"

#. Draw boundary lines
msgid "Temperature Thresholds"
msgstr ""

msgid "Critical"
msgstr ""

msgid "Low"
msgstr ""

#, python-format
msgid "Temperature (%s)"
msgstr ""

msgid "Processor"
msgstr ""

#~ msgid "Available entropy"
#~ msgstr "dostępna entropia"

#~ msgid "Load average  1m"
#~ msgstr "Załadowania średnią 1min"

#~ msgid "Load average  5m"
#~ msgstr "Załadowania średnią 5min"

#~ msgid "Load average 15m"
#~ msgstr "Załadowania średnią 15min"

#~ msgid "CPU usage"
#~ msgstr "użycie CPU"
